package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class LoginPageLocators<data> {

    private By usernameLocator = By.id("username");
    private By passwordLocator = By.id("password");
    private By loginLocator = By.id("enter");
    private By FailedLoginLocator = By.xpath(".//p[@id='rejectLogin']/b");


}
