package com.safebear.auto.pages;


import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;


@RequiredArgsConstructor
public class LoginPage {

    LoginPageLocators locators = new LoginPageLocators();

    @NonNull
    WebDriver driver;

    String expectedpagetitle = "Login Page";

    public String getExpectedpagetitle() {
        return expectedpagetitle;
    }

    public String getPageTitle() {
        return driver.getTitle();
    }

    public void enterUsername(String username) {
        driver.findElement(locators.getUsernameLocator()).sendKeys(username);
    }

    public void enterPassword(String password) {
        driver.findElement(locators.getPasswordLocator()).sendKeys(password);
    }

    public void clickLoginButton() {
        driver.findElement(locators.getLoginLocator()).click();
    }

    public String checkForFailedLoginWarning() {
        return driver.findElement(locators.getFailedLoginLocator()).getText();
    }

    //below I have made a method that links multiple methods. The first part says the name "login" and then the data entries required
    //The items underneath are the methods being called and what their data elements are
    public void login(String username, String password) {
        enterUsername(username);
        enterPassword(password);
        clickLoginButton();


    }

}