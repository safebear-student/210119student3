package com.safebear.auto.tests;

import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterClass;

//making any change so I can commit and push
@CucumberOptions(
        //plugin = {"pretty", "html:target/cucumber"},
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"},
        tags = "~@to-do",
        glue = "com.safebear.auto.tests",
        features = "classpath:toolslist.feature/login.feature"
)
public class
RunCukes extends AbstractTestNGCucumberTests {

    @AfterClass
    public static void writeExtentReport() {
        Reporter.loadXMLConfig("src/extent-config.xml");
    }
}
