package com.safebear.auto.nonBddTests;


import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    @Test
    public void validLogin() {

        //1. Action to go to Login page
        driver.get(Utils.getUrl());

        //1. Expected check we're on the login page
        Assert.assertEquals(loginPage.getExpectedpagetitle(), loginPage.getExpectedpagetitle());

        //2. Action to enter valid user credentials
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();

        //capture scrrenshot
        Utils.capturescreenshot(driver,Utils.generateScreenShotFileName("Login Success"));

        //2. Expected check success message
        Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page");

        //3. Expected Check success message is shown
        Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains("Success"));

    }

    @Test
    public void invalidLogin() {
        //1. Action to go to Login page
        driver.get(Utils.getUrl());

        //1. Expected check we're on the login page
        Assert.assertEquals(loginPage.getExpectedpagetitle(), loginPage.getExpectedpagetitle());

        //2. Action to enter valid user credentials
        loginPage.login("badlogin","letmein");

        //2. Expected check failure message
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page");

        //3. Expected Check failure message is shown
        Assert.assertTrue(loginPage.checkForFailedLoginWarning().contains("incorrect "));

        //capture scrrenshot
        Utils.capturescreenshot(driver,Utils.generateScreenShotFileName("Login failure "));
    }

}
