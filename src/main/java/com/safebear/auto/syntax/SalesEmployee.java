package com.safebear.auto.syntax;

public class SalesEmployee extends Employee {

    private String car;

    public String getCar() {
        return car;
    }

    public void changeCar(String newCar) {
        car = newCar;
    }

}
